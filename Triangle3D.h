#ifndef TRIANGLE3D_H_
#define TRIANGLE3D_H_

#include <QColor>
#include <string>
#include <iostream>
#include <clocale>

class Triangle3D{
public:
    float x1, y1, z1, x2, y2, z2, x3, y3, z3;
    unsigned char color[4]; //r, g, b, a
    Triangle3D();
    Triangle3D(const Triangle3D& cpy);
    Triangle3D(float x1, float y1, float z1, float x2, float y2, float z2, float x3, float y3, float z3);
    Triangle3D(float x1, float y1, float z1, float x2, float y2, float z2, float x3, float y3, float z3,
      unsigned char r, unsigned char g, unsigned char b, unsigned char a=255);
    Triangle3D(std::string s); //Deserialization
    void setColor(unsigned char r, unsigned char g, unsigned char b, unsigned char a=255);
    QColor getQColor();
    Triangle3D* add(float x, float y, float z);
};

#endif
