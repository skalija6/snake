#ifndef MODELLOADER_H_
#define MODELLOADER_H_

#include "Triangle3D.h"
#include <fstream>
#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <dirent.h>
#include <string.h>

class ModelLoader {
    std::map<std::string, std::vector<Triangle3D*> > models;
    std::vector<std::string> getModelFiles();
    static bool endsWith(const char* str, const char* end);
    void parseFile(const std::string &fileName);
public:
    ModelLoader();
    ~ModelLoader();
    
    const std::vector<Triangle3D*>& getModel(const std::string name);
    
};

#endif