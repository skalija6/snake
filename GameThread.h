#ifndef GAMETHREAD_H_
#define GAMETHREAD_H_


#include <QThread>
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <time.h>
#include "DataStorage.h"

class GameThread : public QThread {
    Q_OBJECT
    
    void run() override;
    
    
public:
    GameThread(QWidget* parent = 0);
    ~GameThread();
    
};

#endif