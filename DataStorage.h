#ifndef DATASTORAGE_H_
#define DATASTORAGE_H_

#include <QMutex>
#include "ScreenWidget.h"
#include "CanvasWidget.h"
#include "GameThread.h"
#include "SnakeData.h"
#include "Triangle3D.h"
#include "ModelLoader.h"
#include "FoodObject.h"
#include <vector>
#include <chrono>

class ScreenWidget;
class CanvasWidget;
class GameThread;
class Triangle3D;
class FoodObject;

class DataStorage {
    static DataStorage *instance;
    ScreenWidget *screen;
    CanvasWidget *canvas;
    GameThread *gameThread;
    ModelLoader* modelLoader;
    unsigned int randomSeed;
    unsigned short menuState;
    
    const int SLOWNESS_POWERUP_TICKS = 110;
    
    //synchronized
    QMutex mutex;
    int sync; //testing
    int viewX;
    int viewY;
    int moveDir;
    int playerScore;
    float playerSpeed;
    int playerPosX;
    int playerPosY;
    int playerLength;
    SnakeData* snakeData;
    int gameState;
    std::vector<FoodObject*> foodObjects;
    int addParts;
    bool playerHasSlownessPowerup;
    int slownessPowerupActiveTicksLeft;
    
    void addLocalFoodObject(int x = -99999, int y = -99999);
    int nextRandomInt();

public:
    DataStorage();
    static DataStorage* getInstance();
    
    void setScreenWidgetInstance(ScreenWidget* sw);
    ScreenWidget* getScreenWidgetInstance();
    
    void setCanvasWidgetInstance(CanvasWidget* sw);
    CanvasWidget* getCanvasWidgetInstance();
    
    void setGameThreadInstance(GameThread* gt);
    GameThread* getGameThreadInstance();
    
    ModelLoader* getModelLoader();
    
    std::string getText();
    
    unsigned short& getMenuState();
    
    //synchronized
    int increment();
    int get();
    void clearTick();
    
    void initSnakeData();
    void updateView(int dx, int dy);
    std::pair<int,int> getView();
    std::pair<int,int> getFacing();
    void updateFacing();
    std::pair<int,int> getPlayerPos();
    void setPlayerPos(int x, int y);
    void updatePlayerPos(int dx, int dy);
    void updateSnakeData(int direction, int speed);
    int getPlayerSpeed(bool gameTick = true);
    void debugResetPlayerSpeed();
    std::vector<Triangle3D*> getSnakeModel();
    void setGameState(int s);
    int getGameState();
    void checkCollisions();
    void addFoodObject(int x = -99999, int y = -99999);
    void clearFoodObjects();
    std::vector<Triangle3D*> getFoodModel();
    int getFoodObjectCount();
    int getScore();
    bool hasSlownessPowerup();
    void claimSlownessPowerup();
    double getSlownessPowerupDuration();
};

#endif