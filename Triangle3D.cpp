#include "Triangle3D.h"

Triangle3D::Triangle3D() : x1(0), y1(0), x2(0), y2(0), x3(0), y3(0) {
    for(int i=0; i<4; i++) this->color[i] = 255;
}

Triangle3D::Triangle3D(const Triangle3D& cpy){
    this->x1 = cpy.x1;
    this->y1 = cpy.y1;
    this->z1 = cpy.z1;
    this->x2 = cpy.x2;
    this->y2 = cpy.y2;
    this->z2 = cpy.z2;
    this->x3 = cpy.x3;
    this->y3 = cpy.y3;
    this->z3 = cpy.z3;
    for(int i=0; i<4; i++) this->color[i] = cpy.color[i];
}

Triangle3D::Triangle3D(float x1, float y1, float z1, float x2, float y2, float z2, float x3, float y3, float z3)
  : x1(x1), y1(y1), z1(z1), x2(x2), y2(y2), z2(z2), x3(x3), y3(y3), z3(z3) {
    for(int i=0; i<4; i++) this->color[i] = 255;
}

Triangle3D::Triangle3D(float x1, float y1, float z1, float x2, float y2, float z2, float x3, float y3, float z3, unsigned char r,
      unsigned char g, unsigned char b, unsigned char a) : x1(x1), y1(y1), z1(z1), x2(x2), y2(y2), z2(z2), x3(x3), y3(y3), z3(z3) {
    this->setColor(r, g, b, a);
}

Triangle3D::Triangle3D(std::string s){
    std::setlocale(LC_NUMERIC, "en_US.UTF-8");
    //Deserialization
    //x1,y1,z1;x2,y2,z2;x3,y3,z3;r,g,b,a
    size_t nxt = -1;
    x1 = std::stof(s, &nxt);
    std::cout << "s[" << nxt << "] = " << s[nxt] << " s = " << s << std::endl;
    if(s[nxt] != ',') throw std::invalid_argument("bad delimiter1");
    s = s.substr(nxt+1);
    y1 = std::stof(s, &nxt);
    std::cout << "s[" << nxt << "] = " << s[nxt] << " s = " << s << std::endl;
    if(s[nxt] != ',') throw std::invalid_argument("bad delimiter2");
    s = s.substr(nxt+1);
    z1 = std::stof(s, &nxt);
    if(s[nxt] != ';') throw std::invalid_argument("bad delimiter3");
    
    s = s.substr(nxt+1);
    x2 = std::stof(s, &nxt);
    if(s[nxt] != ',') throw std::invalid_argument("bad delimiter4");
    s = s.substr(nxt+1);
    y2 = std::stof(s, &nxt);
    if(s[nxt] != ',') throw std::invalid_argument("bad delimiter5");
    s = s.substr(nxt+1);
    z2 = std::stof(s, &nxt);
    if(s[nxt] != ';') throw std::invalid_argument("bad delimiter6");
    
    s = s.substr(nxt+1);
    x3 = std::stof(s, &nxt);
    if(s[nxt] != ',') throw std::invalid_argument("bad delimiter7");
    s = s.substr(nxt+1);
    y3 = std::stof(s, &nxt);
    if(s[nxt] != ',') throw std::invalid_argument("bad delimiter8");
    s = s.substr(nxt+1);
    z3 = std::stof(s, &nxt);
    if(s[nxt] != ';') throw std::invalid_argument("bad delimiter9");
    
    for(int i=0; i<4; i++) this->color[i] = 255;
    s = s.substr(std::min(nxt+1, s.length()));
    
    if(s.length() > 0){
        color[0] = std::stoi(s, &nxt);
        s = s.substr(std::min(nxt+1, s.length()));
    }
    if(s.length() > 0){
        color[1] = std::stoi(s, &nxt);
        s = s.substr(std::min(nxt+1, s.length()));
    }
    if(s.length() > 0){
        color[2] = std::stoi(s, &nxt);
        s = s.substr(std::min(nxt+1, s.length()));
    }
    if(s.length() > 0){
        color[3] = std::stoi(s, &nxt);
    }
    
    std::cout << "Successfully loaded Triangle3D from string. x1=" << x1 << ", y1=" << y1 << ", z1=" << z1
     << ", x2=" << x2 << ", y2=" << y2 << ", z2=" << z2 << ", x3=" << x3 << ", y3=" << y3 << ", z3=" << z3
     << ", r=" << (int)color[0] << ", g=" << (int)color[1] << ", b=" << (int)color[2] << ", a=" << (int)color[3] << std::endl;
}

void Triangle3D::setColor(unsigned char r, unsigned char g, unsigned char b, unsigned char a){
    this->color[0] = r;
    this->color[1] = g;
    this->color[2] = b;
    this->color[3] = a;
}

QColor Triangle3D::getQColor(){
    return QColor(this->color[0], this->color[1], this->color[2], this->color[3]);
}


Triangle3D* Triangle3D::add(float x, float y, float z){
    this->x1 += x;
    this->x2 += x;
    this->x3 += x;
    this->y1 += y;
    this->y2 += y;
    this->y3 += y;
    this->z1 += z;
    this->z2 += z;
    this->z3 += z;
    return this;
}

