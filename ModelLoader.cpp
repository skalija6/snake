#include "ModelLoader.h"

ModelLoader::ModelLoader(){
    std::vector<std::string> modelFiles = this->getModelFiles();
    for(std::string mf : modelFiles){
        parseFile(mf);
    }
    //Printing models
    for(std::pair<std::string, std::vector<Triangle3D*> > model : this->models){
        std::cout << " > Model: " << model.first << " (" << model.second.size() << " triangles)" << std::endl;
        /*for(Triangle3D* t : model.second){
            std::cout << "   > " << t->x1 << ", " << t->y1 << ", " << t->z1 << "..." << std::endl;
        }*/
    }
}

bool ModelLoader::endsWith(const char* str, const char* end){
    size_t len = strlen(str);
    size_t endlen = strlen(end);
    if(endlen >= len) return false; // => -- must end, not equal
    for(size_t i = len-endlen, j = 0; i < len; i++, j++){
        if(str[i] != end[j]) return false;
    }
    return true;
}

std::vector<std::string> ModelLoader::getModelFiles(){
    std::vector<std::string> list;
    DIR *dir;
    struct dirent *ent;
    if((dir = opendir("../models")) != NULL){ //When executable is in subdirectory
        while((ent = readdir(dir)) != NULL){
            if(endsWith(ent->d_name, ".model")) list.push_back("../models/"+std::string(ent->d_name));
        }
        closedir(dir);
    }
    if((dir = opendir("./models")) != NULL){ //When executable is in project root directory
        while((ent = readdir(dir)) != NULL){
            if(endsWith(ent->d_name, ".model")) list.push_back("./models/"+std::string(ent->d_name));
        }
        closedir(dir);
    }
    
    return list;
}

void ModelLoader::parseFile(const std::string &fileName){
    std::string modelName = "";
    size_t len = fileName.length();
    for(int i = ((int)len)-7; i >= 0; i--){
        if(fileName[i] == '/') break;
        modelName = fileName[i] + modelName;
    }
    std::cout << "Model name is " << modelName << "!" << std::endl;
    std::string s;
    std::fstream fs(fileName);
    this->models.insert(std::pair<std::string, std::vector<Triangle3D*> >(modelName, std::vector<Triangle3D*>()));
    while(std::getline(fs, s)){
        //Parse line to Triangle3D
        //...
        std::cout << " - " << s << std::endl;
        try{
            Triangle3D* newTriangle = new Triangle3D(s);
            this->models[modelName].push_back(newTriangle);
        }catch(std::invalid_argument e){
            std::cout << "Exception caught: " << e.what() << std::endl;
        }
    }
    fs.close();
}

const std::vector<Triangle3D*>& ModelLoader::getModel(const std::string name){
    return this->models[name];
}

ModelLoader::~ModelLoader(){}
