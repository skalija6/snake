#include "Triangle.h"

Triangle::Triangle() : x1(0), y1(0), x2(0), y2(0), x3(0), y3(0) {
    for(int i=0; i<4; i++) this->color[i] = 255;
}

Triangle::Triangle(float x1, float y1, float x2, float y2, float x3, float y3)
  : x1(x1), y1(y1), x2(x2), y2(y2), x3(x3), y3(y3) {
    for(int i=0; i<4; i++) this->color[i] = 255;
}

Triangle::Triangle(float x1, float y1, float x2, float y2, float x3, float y3, unsigned char r, unsigned char g,
      unsigned char b, unsigned char a) : x1(x1), y1(y1), x2(x2), y2(y2), x3(x3), y3(y3) {
    this->setColor(r, g, b, a);
}

void Triangle::setColor(unsigned char r, unsigned char g, unsigned char b, unsigned char a){
    this->color[0] = r;
    this->color[1] = g;
    this->color[2] = b;
    this->color[3] = a;
}

QColor Triangle::getQColor(){
    return QColor(this->color[0], this->color[1], this->color[2], this->color[3]);
}