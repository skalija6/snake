#ifndef SMARTMEASURE_H_
#define SMARTMEASURE_H_

//Compatibility for MinGW
/*#ifdef __MINGW32__
  #define gettimeofday mingw_gettimeofday
#endif*/

#include <sys/time.h>
#include <time.h>
#include <string>
#include <vector>
#include <iostream>

struct TimePoint{
  long us;
  std::string name;
  float percentage;
  TimePoint(std::string s, long l) : us(l), name(s) {}
  TimePoint(const TimePoint& t){ //copy constructor
    this->us = t.us;
    this->name = t.name;
    this->percentage = t.percentage;
  }
};

class SmartMeasure{
  long us_from;
  long us_totstart;
  std::vector<TimePoint*> points;
  long us_totalsum;
public:
  SmartMeasure();
  void start();
  void stop();
  void point(const std::string& name);
  std::vector<TimePoint*> getPoints();
  std::vector<TimePoint*> getPercentages();
  ~SmartMeasure();
};

#endif