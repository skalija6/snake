#include <QWidget>
#include <QPushButton>
#include <QDebug>
#include <QVBoxLayout>

#ifndef MENUWIDGET_H_
#define MENUWIDGET_H_

class MenuWidget : public QWidget {
    Q_OBJECT
    
    QVBoxLayout* v;
public:
    MenuWidget(QWidget* parent = 0);
    QVBoxLayout *getLayout();
    
public slots:
    void startGame();
    void handleClick();
    void handleResize();
    
protected:
    void resizeEvent(QResizeEvent *event);

};

#endif