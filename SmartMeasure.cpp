#include "SmartMeasure.h"

SmartMeasure::SmartMeasure(){
  us_totalsum = 0;
}

void SmartMeasure::start(){
  struct timeval tp;
  gettimeofday(&tp, nullptr);
  us_from = tp.tv_sec * 1000000L + tp.tv_usec;
  us_totstart = us_from;
}

void SmartMeasure::stop(){
  struct timeval tp;
  gettimeofday(&tp, nullptr);
  long us_now = tp.tv_sec * 1000000L + tp.tv_usec;
  us_totalsum += us_now-us_totstart;
  //std::cout << "us_totalsum += " << us_now-us_totstart << " (now " << us_totalsum << ")" << std::endl;
}

void SmartMeasure::point(const std::string& name){
  struct timeval tp;
  gettimeofday(&tp, nullptr);
  long us_now = tp.tv_sec * 1000000L + tp.tv_usec;
  long us_add = us_now-us_from;
  
  bool already_in = false;
  for(TimePoint*& tp : points){
    if(tp->name == name){
      already_in = true;
      tp->us += us_add;
    }
  }
  if(!already_in){
    TimePoint* point = new TimePoint(name, us_add);
    points.push_back(point);
  }
  us_from = us_now;
}

std::vector<TimePoint*> SmartMeasure::getPoints(){
  TimePoint* point = new TimePoint("_TOTAL", us_totalsum);
  points.push_back(point);
  return points;
}

std::vector<TimePoint*> SmartMeasure::getPercentages(){
  std::vector<TimePoint*> cpy = points;
  for(TimePoint*& p : cpy){
    p->percentage = 100.0 * p->us / us_totalsum;
  }
  return cpy;
}

SmartMeasure::~SmartMeasure(){
  for(TimePoint* p : points){
    delete p;
  }
  points.clear();
}
