#include "CanvasWidget.h"

CanvasWidget::CanvasWidget(QWidget* parent) : QWidget(parent), tick(0), fps(0), lastFpsShown(0), fpsFinal(0), tpsFinal(0), ticksLast(0){
    this->fpsTarget = 60;
    DataStorage::getInstance()->getMenuState() = 0;
    this->debugType = 0;
    //generate arena triangles
    const int tileSize = 800;
    //floor
    unsigned char colors[] = {128, 255, 90, 188, 255, 130};
    for(int r=0; r<30; r++){
        for(int c=0; c<30; c++){
            unsigned char i1 = 3*((r+c)%2);
            unsigned char i2 = 3-i1;
            arenaTriangles.push_back(new Triangle3D( (c*tileSize), (r*tileSize), -2000, (c*tileSize)+tileSize, (r*tileSize), -2000, (c*tileSize), (r*tileSize)+tileSize, -2000, colors[0+i1], colors[1+i1], colors[2+i1] ));
            arenaTriangles.push_back(new Triangle3D( (c*tileSize)+tileSize, (r*tileSize), -2000, (c*tileSize)+tileSize, (r*tileSize)+tileSize, -2000, (c*tileSize), (r*tileSize)+tileSize, -2000, colors[0+i2], colors[1+i2], colors[2+i2] ));
            //float phi = atan2((float)(r*tileSize), (float)(c*tileSize));
            //std::cout << "XY phi = " << phi;
        }
    }
    //walls
    for(int r=0; r<30; r++){
        for(int i=0; i<4; i++){
            int x1, y1, x2, y2;
            if(i==0){x1 = r*tileSize; y1 = 30*tileSize; x2 = x1+tileSize; y2 = y1;}
            if(i==1){x1 = 30*tileSize; y1 = (30-r)*tileSize; x2 = x1; y2 = y1-tileSize;}
            if(i==2){x1 = (30-r)*tileSize; y1 = 0; x2 = x1-tileSize; y2 = y1;}
            if(i==3){x1 = 0; y1 = r*tileSize; x2 = x1; y2 = y1+tileSize;}
            arenaTriangles.push_back(new Triangle3D( x1, y1, -2000, x2, y2, -2000, x2, y2, tileSize-2000, 120, 60, 30 ));
            arenaTriangles.push_back(new Triangle3D( x2, y2, tileSize-2000, x1, y1, tileSize-2000, x1, y1, -2000, 125, 65, 30 ));
        }
    }
    //lastMouseX = 0;
    //lastMouseY = 0;
    this->setMouseTracking(true);
    this->cameraHeight = 0;
    
    //thread
    GameThread* gt = new GameThread(DataStorage::getInstance()->getScreenWidgetInstance());
    connect(gt, &GameThread::finished, gt, &QObject::deleteLater);
    DataStorage::getInstance()->setGameThreadInstance(gt);
    
    //invisible cursor
    this->setCursor(Qt::BlankCursor);
    
    //keyboard
    this->grabKeyboard();
    
    this->e = -1500;
    this->tweak = false;
    this->doNotMoveMouse = false;
    this->lastMouseX = -400000; //Random Big Integer
    this->lastMouseY = -400000;
    
    this->sm = new SmartMeasure();
}

void CanvasWidget::paintEvent(QPaintEvent *event){
    DataStorage* ds = DataStorage::getInstance();
    int ticksAlive = ds->get();
    this->sm->start();
    this->tick++;
    this->fps++;
    std::chrono::milliseconds now_ms = std::chrono::duration_cast<std::chrono::milliseconds>(
        std::chrono::system_clock::now().time_since_epoch());
    if(now_ms-this->lastFpsShown > std::chrono::milliseconds(1000)){
        fpsFinal = this->fps;
        this->fps = 0;
        int nowTick = ds->get();
        this->tpsFinal = nowTick-this->ticksLast;
        this->ticksLast = nowTick;
        this->lastFpsShown = now_ms;
    }
    
    int sizeX = this->width();
    int sizeY = this->height();
    int centerX = sizeX/2;
    int centerY = sizeY/2;
    
    //Rendering
    //const float e = -1500;
    /*if(ticksAlive%50 == 0)*/ sm->point("Start");
    //1. moving relative to player
    std::pair<int,int> playerPos = ds->getPlayerPos();
    std::vector<Triangle3D*> workVect;
    std::vector<Triangle3D*> foodModel = ds->getFoodModel();
    std::vector<Triangle3D*> snakeModel = ds->getSnakeModel();
    if(this->tweak) workVect.reserve(this->arenaTriangles.size() + foodModel.size() + snakeModel.size() );
    // - arena triangles
    for(Triangle3D* t : this->arenaTriangles){
        Triangle3D* nt = new Triangle3D(*t);
        workVect.push_back(nt->add(-playerPos.first, -playerPos.second, -this->cameraHeight));
    }
    // - food triangles
    for(Triangle3D* t : foodModel){
        Triangle3D* nt = new Triangle3D(*t);
        workVect.push_back(nt->add(-playerPos.first, -playerPos.second, -this->cameraHeight));
    }
    for(Triangle3D* t : foodModel){
        delete t; //anti memory leak
    }
    foodModel.clear();
    // - snake triangles
    for(Triangle3D* t : snakeModel){
        Triangle3D* nt = new Triangle3D(*t);
        workVect.push_back(nt->add(-playerPos.first, -playerPos.second, -this->cameraHeight-400));
    }
    for(Triangle3D* t : snakeModel){
        delete t; //anti memory leak
    }
    snakeModel.clear();
    /*if(ticksAlive%50 == 0)*/ sm->point("Trnsltion");
    //2. rotating
    std::pair<int,int> viewXY = ds->getView();
    float alpha = viewXY.first/100.0;
    float sinA = sin(alpha);
    float cosA = cos(alpha);
    float beta = viewXY.second/100.0;
    float sinB = sin(beta);
    float cosB = cos(beta);
    for(Triangle3D* &t : workVect){
        float newX, newY, newZ;
        //rotate on XY plane
        newX = t->x1*cosA - t->y1*sinA;
        newY = t->x1*sinA + t->y1*cosA;
        t->x1 = newX; t->y1 = newY;
        newX = t->x2*cosA - t->y2*sinA;
        newY = t->x2*sinA + t->y2*cosA;
        t->x2 = newX; t->y2 = newY;
        newX = t->x3*cosA - t->y3*sinA;
        newY = t->x3*sinA + t->y3*cosA;
        t->x3 = newX; t->y3 = newY;
        
        //rotate on XZ plane
        newX = t->x1*cosB + t->z1*sinB;
        newZ = -t->x1*sinB + t->z1*cosB;
        t->x1 = newX; t->z1 = newZ;
        newX = t->x2*cosB + t->z2*sinB;
        newZ = -t->x2*sinB + t->z2*cosB;
        t->x2 = newX; t->z2 = newZ;
        newX = t->x3*cosB + t->z3*sinB;
        newZ = -t->x3*sinB + t->z3*cosB;
        t->x3 = newX; t->z3 = newZ;
        
        
        /*float phiXY = atan2((float)t->y1, (float)t->x1);
        float distXY = sqrt(t->x1*t->x1 + t->y1*t->y1);
        float phi = atan2((float)t->z1, (float)t->x1);
        float dist = sqrt(t->x1*t->x1 + t->z1*t->z1);
        std::cout << "XY phi = " << phiXY << " XY dist = " << distXY 
          << " XZ phi = " << phi << " XZ dist = " << dist << std::endl;
        std::cout << "3Dpos " << (int)t->x1<<";"<<(int)t->y1<<";"<<(int)t->z1<<" / " << (int)t->x2<<";"<<(int)t->y2<<";"<<(int)t->z2<<" / "
          << (int)t->x3<<";"<<(int)t->y3<<";"<<(int)t->z3<<" / " << std::endl;*/
        
        
    }
    /*if(ticksAlive%50 == 0)*/ sm->point("Rotation");
    //2,5. optimization - do not show triangles facing away from camera
    for(Triangle3D* &t : workVect){
        float v1x = t->x2 - t->x1;
        float v1y = t->y2 - t->y1;
        float v1z = t->z2 - t->z1;
        float v2x = t->x3 - t->x1;
        float v2y = t->y3 - t->y1;
        float v2z = t->z3 - t->z1;
        //normal vector - perpendicular to the triangle
        float vkx = v1y * v2z - v1z * v2y;
        float vky = v1z * v2x - v1x * v2z;
        float vkz = v1x * v2y - v1y * v2x;
        float dotproduct = vkx*t->x1 + vky*t->y1 + vkz*t->z1; //~ cosine of angle
        if(dotproduct > 0) t->x1 = -1;//do not show the triangle
    }
    /*if(ticksAlive%50 == 0)*/ sm->point("B-fc cull");
    
    //3. perspective (3D -> 2D)
    std::vector<Triangle*> renderVect;
    const int cc = 3;
    
    for(Triangle3D* t : workVect){
        float newX, newY;
        if(t->x1 < 1 || t->x2 < 1 || t->x3 < 1) continue; //distance clamping
        //if(t->x1 < 10 && t->x2 < 10 && t->x3 < 10) continue; //distance clamping
        const int maxDistToRender = 40000;
        if(t->x1 > maxDistToRender || t->x2 > maxDistToRender || t->x3 > maxDistToRender) continue; //distance clamping
        
        Triangle* newT = new Triangle();
        
        newX = e*t->y1 / t->x1;
        newY = e*t->z1 / t->x1;
        newT->x1 = (newX/cc+centerX); newT->y1 = (newY/cc+centerY);
        newX = e*t->y2 / t->x2;
        newY = e*t->z2 / t->x2;
        newT->x2 = (newX/cc+centerX); newT->y2 = (newY/cc+centerY);
        newX = e*t->y3 / t->x3;
        newY = e*t->z3 / t->x3;
        newT->x3 = (newX/cc+centerX); newT->y3 = (newY/cc+centerY);
        
        //optimization - do not show triangles away from canvas
        if((newT->x1 < 0 && newT->x2 < 0 && newT->x3 < 0) || (newT->x1 > sizeX && newT->x2 > sizeX && newT->x3 > sizeX)){
            delete newT;
            continue;
        }
        if((newT->y1 < 0 && newT->y2 < 0 && newT->y3 < 0) || (newT->y1 > sizeY && newT->y2 > sizeY && newT->y3 > sizeY)){
            delete newT;
            continue;
        }
        newT->setColor(t->color[0], t->color[1], t->color[2], t->color[3]);
        renderVect.push_back(newT);
    }
    
    /*if(ticksAlive%50 == 0)*/ sm->point("Perspct");
    
    
    
    //End of rendering
    //Painting
    
    QPainter painter(this);
    painter.setPen(QPen(QColor(0,0,0,0)));
    
    //Painting sky
    {
        QPainterPath path;
        path.moveTo(0, 0);
        path.lineTo(sizeX, 0);
        path.lineTo(sizeX, sizeY);
        path.lineTo(0, sizeY);
        path.closeSubpath();
        path.setFillRule(Qt::WindingFill);
        //painter.setBrush(QBrush(QColor(0, 0, 0, 255)));
        painter.setBrush(QBrush(QColor(142, 218, 255, 255)));
        painter.drawPath(path);
    }

    /*if(ticksAlive%50 == 0)*/ sm->point("Paint sky");
    
    //painter.setRenderHint(QPainter::Antialiasing); //this was causing 1px blank spaces between triangles
    //painter.setBrush(QBrush(QColor(30,180,this->tick%256)));
    painter.setPen(QPen(QColor(0,0,0,this->tweak*255)));
    //Painting triangles
    for(Triangle* t : renderVect){
        QPainterPath path;
        /*std::cout << t->x1<<";"<<t->y1<<"  " << t->x2<<";"<<t->y2<<"  " << t->x3<<";"<<t->y3<<"  " <<(int)t->color[0]<<", "
          <<(int)t->color[1]<<", "<<(int)t->color[2]<<", "<<(int)t->color[3]<<std::endl;*/
        path.moveTo((int)t->x1, (int)t->y1);
        path.lineTo((int)t->x2, (int)t->y2);
        path.lineTo((int)t->x3, (int)t->y3);
        path.closeSubpath();
        path.setFillRule(Qt::WindingFill);
        painter.setBrush(QBrush(t->getQColor()));
        painter.drawPath(path);
    }
    
    //Painting menu
    unsigned short& menuState = ds->getMenuState();
    if(menuState){
        std::vector<std::string> menuItems;
        menuItems.push_back("Nastavení [ESC]");
        menuItems.push_back(std::string("FOV: ")+std::to_string((int)this->e));
        menuItems.push_back(std::string("Snímky za sekundu: ")+(this->fpsTarget == 6000 ? "MAX" : std::to_string(this->fpsTarget)));
        menuItems.push_back(std::string("Myš: ")+(this->doNotMoveMouse ? "VIDITELNÁ" : "NEVIDITELNÁ"));
        menuItems.push_back(std::string("Ladění: ")+(debugType ? (debugType>1 ? "MAXIMÁLNÍ" : "JEDNODUCHÉ") : "VYPNUTO"));
        
        const int menuX = 8;
        const int menuY = 200;
        const int menuWidth = 230;
        const int menuHeight = 40;
        
        int i=0;
        
        for(std::string item : menuItems){
        
            int startX = menuX;
            int startY = menuY+(menuHeight+2)*i;
            
            bool selectedItem = false;
            
            if(i == menuState){
                selectedItem = true;
            }
        
            painter.setPen(QPen(QColor(0,0,0,0)));
            QPainterPath path;
            path.moveTo(startX, startY);
            path.lineTo(startX+menuWidth, startY);
            path.lineTo(startX+menuWidth, startY+menuHeight);
            path.lineTo(startX, startY+menuHeight);
        
            path.closeSubpath();
            path.setFillRule(Qt::WindingFill);
            painter.setBrush((selectedItem ? QColor(238, 60, 7) : QColor(60, 60, 65)));
            painter.drawPath(path);
        
            painter.setPen(QPen(QColor(255,255,255,190)));
            QFont font = painter.font();
            font.setPixelSize(18);
            painter.setFont(font);
            painter.drawText(startX+5, startY+27, QString(item.c_str()));
            font.setPixelSize(13);
            painter.setFont(font);
            
            i++;
        
        }
    }
    
    /*if(ticksAlive%50 == 0)*/ sm->point("Draw tris");
    
    std::pair<int,int> facingXY = ds->getFacing();
    std::pair<int,int> posXY = ds->getPlayerPos();
    
    painter.setPen(QPen(QColor(0,0,0,255)));
    int gameState = ds->getGameState();
    
    if(this->debugType >= 1){
        QFont font = painter.font();
        font.setPixelSize(13);
        painter.setFont(font);
        painter.drawText(5, 20, QString("FPS: %1, TPS: %2, triangles: %3, camera height: %4, tick: {%5,%6}, view [%7,%8] facing [%9], pos [%10, %11], gameState: %12, food: %13, e: %14, tweak: %15, mouse: %16, menu: %17, speed: %18").arg(fpsFinal).arg(tpsFinal).arg(renderVect.size())
          .arg(this->cameraHeight).arg(ticksAlive).arg(this->tick).arg(viewXY.first).arg(viewXY.second).arg(facingXY.first).arg(posXY.first).arg(posXY.second).arg(gameState).arg(ds->getFoodObjectCount()).arg(e).arg(this->tweak).arg(this->doNotMoveMouse).arg(ds->getMenuState()).arg(ds->getPlayerSpeed(false)));
    }
    
    //Painting powerup indicator
    bool playerHasSlownessPowerup = ds->hasSlownessPowerup();
    QColor background = (playerHasSlownessPowerup ? QColor(140, 170, 250) : QColor(200, 200, 200, 100) );
    QColor foreground = (playerHasSlownessPowerup ? QColor(70, 110, 190) : QColor(150, 150, 150, 50) );
    painter.setBrush(QBrush(background));
    painter.setPen(QPen(QColor(0,0,0,0)));
    painter.drawEllipse(sizeX-90, 30, 60, 60);
    int midX = sizeX-60;
    int midY = 60;
    painter.setBrush(QBrush(foreground));
    QPoint upper[3];
    upper[0].setX(midX-15);
    upper[0].setY(midY-15);
    upper[1].setX(midX+15);
    upper[1].setY(midY-15);
    upper[2].setX(midX);
    upper[2].setY(midY);
    painter.drawConvexPolygon(upper, 3);
    QPoint lower[3];
    lower[0].setX(midX-15);
    lower[0].setY(midY+15);
    lower[1].setX(midX+15);
    lower[1].setY(midY+15);
    lower[2].setX(midX);
    lower[2].setY(midY);
    painter.drawConvexPolygon(lower, 3);
    
    double duration = ds->getSlownessPowerupDuration();
    QPen pen(QColor(255, 255, 255, (duration > 0 ? 200 : 0)));
    pen.setWidth(3);
    painter.setPen(pen);
    QRectF rectangle(midX-30, midY-30, 60.0, 60.0);
    painter.drawArc(rectangle, 1440, 5760*duration);
    
    if(playerHasSlownessPowerup){
        painter.setPen(QPen(QColor(255,255,255,190)));
        QFont font = painter.font();
        font.setPixelSize(48);
        painter.setFont(font);
        painter.drawText(midX-14, midY+17, QString("E"));
        font.setPixelSize(13);
        painter.setFont(font);
    }
    
    //Painting score
    painter.setPen(QPen(QColor(255,255,255,255)));
    QFont font = painter.font();
    font.setPixelSize(20);
    painter.setFont(font);
    painter.drawText(centerX-40, 40, QString("Skóre: %1").arg(ds->getScore()));
    font.setPixelSize(13);
    painter.setFont(font);
    
    painter.setPen(QPen(QColor(0,0,0,255)));
    
    this->sm->stop();
    //SmartMeasure output:
    if(this->debugType >= 2){
        if(this->tick%100 == 2){
            //std::cout << "SmartMeasure restart in tick " << this->tick << std::endl;
            std::vector<TimePoint*> tmp = sm->getPercentages();
            for(TimePoint* tp : pts){
                delete tp;
            }
            pts.clear();
            for(TimePoint* tp : tmp){
                pts.push_back(new TimePoint(*tp));
            }
            if(this->sm != nullptr) delete this->sm;
            this->sm = new SmartMeasure();
        }
        int line = 0;
        for(auto p : pts){
            //if(line == 0) std::cout << "SmartMeasure point drawing in tick " << this->tick << ", us=" << p->us << std::endl;
            int x = 10;
            int y = 38+14*line++;
            painter.drawText(x+210, y+12, 
            QString("%1\t->\t%2\t(%3 %)").arg(p->name.c_str()).arg(p->us).arg(p->percentage));
        
            {
            QPainterPath path;
            path.moveTo(x, y);
            path.lineTo(x+200, y);
            path.lineTo(x+200, y+10);
            path.lineTo(x, y+10);
            path.closeSubpath();
            path.setFillRule(Qt::WindingFill);
            painter.setBrush(QBrush(QColor(70, 70, 70, 255)));
            painter.drawPath(path);
            }
        
            {
            QPainterPath path;
            path.moveTo(x, y);
            path.lineTo(x+(2*p->percentage), y);
            path.lineTo(x+(2*p->percentage), y+10);
            path.lineTo(x, y+10);
            path.closeSubpath();
            path.setFillRule(Qt::WindingFill);
            painter.setBrush(QBrush(QColor((line*3000)%256, (line*2000)%256, (line*689)%256, 255)));
            painter.drawPath(path);
            }
        }
    }
    
    
    //No memory leaks please
    for(Triangle3D* t : workVect){
        delete t;
    }
    for(Triangle* t : renderVect){
        delete t;
    }
    if(gameState == 2){ //Endgame
        this->timer->stop();
        ds->getScreenWidgetInstance()->deleteGame();
        ds->getScreenWidgetInstance()->displayMenu();
    }
}

void CanvasWidget::mouseMoveEvent(QMouseEvent *event){

    int centerX = this->width()/2;
    int centerY = this->height()/2;
    QPoint center = this->mapToGlobal(QPoint(centerX, centerY));
    int deltaX = event->x()-centerX;
    int deltaY = centerY-event->y();
    
    if(this->doNotMoveMouse){
        if(this->lastMouseX == -400000 || this->lastMouseY == -400000){ //not initialized - do not move
            this->lastMouseX = deltaX;
            this->lastMouseY = deltaY;
            deltaX = 0;
            deltaY = 0;
        }else{ //initialized, move by absolute delta
            int oldDeltaX = deltaX;
            int oldDeltaY = deltaY;
            deltaX -= this->lastMouseX;
            deltaY -= this->lastMouseY;
            this->lastMouseX = oldDeltaX;
            this->lastMouseY = oldDeltaY;
        }
    }
    
    DataStorage::getInstance()->updateView(deltaX/3, deltaY/3);
    
    //move mouse cursor to the center
    if(!this->doNotMoveMouse) QCursor::setPos(center);
}

void CanvasWidget::startLoop(){
    const int fps = this->fpsTarget;
    this->timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, QOverload<>::of(&CanvasWidget::repaint));
    timer->start(1000/fps);
    DataStorage* ds = DataStorage::getInstance();
    ds->setGameState(1);
    ds->getGameThreadInstance()->start();
}

CanvasWidget::~CanvasWidget(){
    for(Triangle3D* t : this->arenaTriangles){
        delete t;
    }
    this->arenaTriangles.clear();
    delete this->timer;
}

void CanvasWidget::wheelEvent(QWheelEvent *event){
    int newHeight = this->cameraHeight - event->angleDelta().y();
    if(newHeight < -800) newHeight = -800;
    if(newHeight > 7000) newHeight = 7000;
    this->cameraHeight = newHeight;
    event->accept();
}

void CanvasWidget::keyPressEvent(QKeyEvent *event){
    DataStorage* ds = DataStorage::getInstance();
    std::cout << event->text().toStdString() << " -> " << event->key() << std::endl;
    if(event->text() == "q") e -= 50;
    if(event->text() == "r") e += 50;
    if(event->text() == "e"){
        if(ds->hasSlownessPowerup()){
            ds->claimSlownessPowerup();
            
        }
    }
    //if(event->text() == "w") e -= 10;
    if(event->text() == "t") this->tweak = !this->tweak;
    if(event->text() == "z") ds->debugResetPlayerSpeed();
    if(event->text() == "m"){
        this->doNotMoveMouse = !this->doNotMoveMouse;
        if(this->doNotMoveMouse) this->setCursor(Qt::ArrowCursor);
        else this->setCursor(Qt::BlankCursor);
    }
    //menu
    const int menuItemCount = 4;
    unsigned short& menuState = ds->getMenuState();
    if(event->key() == 16777216){ //ESC
        menuState = !menuState;
    }
    if(event->key() == 16777235){ //UP
        if(menuState >= 1 && menuState <= menuItemCount){
            menuState--;
            if(menuState < 1) menuState += menuItemCount;
        }
    }
    if(event->key() == 16777237){ //DOWN
        if(menuState >= 1 && menuState <= menuItemCount){
            menuState++;
            if(menuState > menuItemCount) menuState -= menuItemCount;
        }
    }
    if(event->key() == 16777220){ //ENTER
        if(menuState == 3){ //Mouse settings
            this->doNotMoveMouse = !this->doNotMoveMouse;
            if(this->doNotMoveMouse) this->setCursor(Qt::ArrowCursor);
            else this->setCursor(Qt::BlankCursor);
        }
    }
    if(event->key() == 16777234){ //LEFT
        if(menuState == 1){ //FOV settings
            e -= 50;
        }
        else if(menuState == 2){ //FPS settings
            int& fps = this->fpsTarget;
            if(fps == 60) fps = 30;
            else if(fps == 30) fps = 20;
            else if(fps == 20) fps = 6000;
            else if(fps == 6000) fps = 144;
            else if(fps == 144) fps = 60;
            this->timer->setInterval(1000/this->fpsTarget);
        }
        else if(menuState == 4){ //Debug settings
            this->debugType--;
            if(this->debugType > 2) this->debugType = 2;
        }
    }
    if(event->key() == 16777236){ //RIGHT
        if(menuState == 1){ //FOV settings
            e += 50;
        }
        else if(menuState == 2){ //FPS settings
            int& fps = this->fpsTarget;
            if(fps == 60) fps = 144;
            else if(fps == 144) fps = 6000;
            else if(fps == 6000) fps = 20;
            else if(fps == 20) fps = 30;
            else if(fps == 30) fps = 60;
            this->timer->setInterval(1000/this->fpsTarget);
        }
        else if(menuState == 4){ //Debug settings
            this->debugType++;
            if(this->debugType > 2) this->debugType = 0;
        }
    }
    
}
