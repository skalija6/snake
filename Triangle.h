#ifndef TRIANGLE_H_
#define TRIANGLE_H_

#include <QColor>

class Triangle{
public:
    float x1, y1, x2, y2, x3, y3;
    unsigned char color[4]; //r, g, b, a
    Triangle();
    Triangle(float x1, float y1, float x2, float y2, float x3, float y3);
    Triangle(float x1, float y1, float x2, float y2, float x3, float y3, unsigned char r, unsigned char g,
      unsigned char b, unsigned char a=255);
    void setColor(unsigned char r, unsigned char g, unsigned char b, unsigned char a=255);
    QColor getQColor();
};

#endif