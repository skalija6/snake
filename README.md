# Snake

Cílem je vytvořit jednoduchou 3D hru, ve které hrajete za hada. Otáčení hada je řešeno podle pohybu myši, cílem hada je žrát ovoce a tím získávat délku, rychlost a skóre.

# Popis technologie

3D renderování je proces, při kterém ze vstupního pole 3D trojúhelníků, které mají 3 trojrozměrné body, které těžko budu kreslit na dvourozměrnou obrazovku, udělám pole 2D trojúhelníků, které už lze jednoduše nakreslit na obrazovku. Tento proces lze rozdělit na několik podúkolů, které vykonám v jasně daném pořadí. Pro účely tohoto projektu jsem navrhl jednoduchý pipeline. Pipeline obsahuje několik podúkolů, z nichž každý má svůj vstup a výstup, do prvního podúkolu je jako vstup pole 3D trojúhelníků, z prvního podúkolu vystupuje pole 2D trojúhelníků připravené k vykreslení na obrazovku. Vstupy ostatních podúkolů jsem napojeny na výstupy předchozího podúkolu. V mém pipelinu jsou tedy následující podúkoly:

## 1. Translace (posuv)

Jako první je vhodné projít všechny trojúhelníky a od všech souřadnic všech jejich bodů odečíst aktuální pozici kamery. Tím posuneme celý svět tak, aby byl jeho počátek v bodě kamery. To je vhodné z důvodu zjednodušení další práce s trojúhelníky.

## 2. Rotace (otočení)

Jako další je nutno provést otočení světa (tj. všech trojúhelníků) podle otočení pohledu kamery. Já otáčím nejprve podle osy Z, poté podle osy Y, vše pomocí jednoduché rotační matice. Rotační matice pro otočení podle osy Z vypadá například takto:
```c++
newX = x*cos(alpha) - y*sin(alpha);
newY = x*sin(alpha) + y*cos(alpha);
```

## 3. Perspektiva

V této fázi je potřeba konečně převést 3D trojúhelníky na 2D trojúhelníky. Zde se využívá perspektivy. Vzdálenější trojúhelníky tedy budou menší než ty bližší. Při vymýšlení funkčního algoritmu jsem se inspiroval lidským okem. Také se v tomto kroku smažou trojúhelníky, které jsou moc blízko kameře (a za kamerou) a které jsou moc daleko od kamery.

![Nákres perspektivy](https://i.imgur.com/h1QH2kp.png)

Každý snímek tedy vytvoří seznam (`std::vector`) obsahující 3D trojúhelníky a tento seznam projde celou pipelinou, ze které vyjde seznam 2D trojúhelníků, které pak za pomocí Qt vykreslím na obrazovku.

# Optimalizace

Herní renderovací enginy by obecně měly být co nejvíce optimalizované, protože potřebují co nejvíce výkonu - není tedy moc prostor na neefektivitu. V mém renderovacím enginu využívám několika optimalizačních technik. Tyto optimalizace vlastně redukují počet trojúhelníků, které se mají vykreslovat. Samozřejmě by nebylo dobré nezobrazovat trojúhelníhy, které bych vidět měl, takže různé optimalizace se snaží smazat ty trojúhelníky, které nebudou vidět. Zároveň je dobré si uvědomit, že každý trojúhelník projde každý snímek celou pipelinou, takže je pro výkon dobré, když zbytečný trojúhelník smažu co nejdříve, aby se s ním zbytečně dále nepočítalo.

## Back-face culling

Tato technika spočívá v tom, že jednoduše maže (nezobrazuje) trojúhelníky, které by kamera viděla zezadu. Například, pokud máme neprůhledný model krychle, tak máme 6 čtverců, neboli 12 trojúhelníků. Ať se na krychli podíváme odkudkoli, nikdy neuvidíme všechny její stěny najednou. Zadní stěny tedy není nutno vykreslovat, protože budou stejně překryty jinými stěnami. To, jestli trojúhelník směřuje směrem ke kameře, nebo ne, jde zjistit poté, co jsou trojúhelníky posunuty a otočeny. Tuto optimalizaci můžeme tedy použít mezi druhým a třetím podúkolem v pipelině. Ušetříme tak výpočty perspektivy značné části trojúhelníků. Směřování ke kameře lze zjistit jednoduchým výpočtem. Je potřeba znát normálový vektor trojúhelníku. To je vektor, který je kolmý k trojúhelníku. Tento vektor lze vypočítat snadno, je potřeba znát dva různé vektory ležící v rovině trojúhelníku, ty lze vytvořit odečtením dvou bodů trojúhelníku. Z těchto dvou vektorů pak vytvoříme normálový vektor známým vzorečkem:

```c++
//v1
float v1x = t->x2 - t->x1;
float v1y = t->y2 - t->y1;
float v1z = t->z2 - t->z1;
//v2
float v2x = t->x3 - t->x1;
float v2y = t->y3 - t->y1;
float v2z = t->z3 - t->z1;
//normalovy vektor
float vkx = v1y * v2z - v1z * v2y;
float vky = v1z * v2x - v1x * v2z;
float vkz = v1x * v2y - v1y * v2x;
```

Jako další je potřeba znát vektor směřující od kamery k trojúhelníku. To je ale velice snadné, protože jsme dříve celý svět posunuli tak, aby kamera byla v počátku. Tento vektor tedy bude složen ze souřadnic trojúhelníku (já zvolil souřadnice prvního bodu trojúhelníku). Známe tedy normálový vektor trojúhelníku (ten, který ukazuje směrem, kterým ukazuje trojúhelník) a vektor ukazující od kamery k trojúhelníku. Jejich odchylka nám ukáže, jestli koukáme na trojúhelník zepředu, nebo zezadu.
`obrazek tbd`
Odchylka dvou vektorů se vypočítá tímto vzorcem:
```
cos(a) = ( u . v ) / ( |u| . |v| )
```
V čitateli je skalární součin obou vektorů a ve jmenovateli je součin délek vektorů. Délka vektorů je ale poměrně náročná operace, protože potřebuje odmocninu. Celý jmenovatel ale násobí dvě délky, tedy kladná čísla. Násobek dvou kladných čísel je opět kladné číslo, celý jmenovatel je tedy kladný. Jmenovatel tedy v žádném případě nezmění znaménko celého zlomku. Funkce cosinus je funkce sudá a od -90 ° do +90 ° je kladná a do -90 ° a od 90 ° záporná, což nám stačí, protože právě pokud je odchylka našich dvou vektorů mezi -90 ° a 90 °, kamera kouká na trojúhelník zezadu, jinak zepředu. K našim výpočtům odchylky nám tedy stačí skalární součin. Pokud je ten kladný, vektory ukazují stejným směrem, pokud je záporný, ukazují opačným směrem. Pokud je skalární součin roven 0, vektory jsou kolmé, a tak trojúhelník také není nutné kreslit. Když normálový vektor trojúhelníku a vektor směřující od kamery k trojúhelníku ukazují stejným směrem, pak kamera kouká na trojúhelník zezadu a není ho tak nutné kreslit. 

```c++
float dotproduct = vkx*t->x1 + vky*t->y1 + vkz*t->z1; //~ kosinus uhlu
if(dotproduct > 0) t->x1 = -1;//nezobrazovat trojuhelnik
```

Nezobrazování trojúhelníku je řešeno tak, že se x-ová souřadnice jednoho bodu trojúhelníku nastaví na -1, tedy za kameru. Ve třetím podúkolu pipeliny je podmínka, při které pokud má nějaký bod trojúhelníku záporné x (je tedy za kamerou), nebude trojúhelník zobrazen.

```c++
if(t->x1 < 1 || t->x2 < 1 || t->x3 < 1) continue; //dokonce pokud je x < 1, trojuhelnik se nezobrazi
```

## Nezobrazování trojúhelníků mimo kreslicí plochu

Každý trojúhelník, který předám Qt k vykreslení na obrazovku, musí projít dalším zpracováním přímo v Qt. Pokud Qt předám trojúhelník, který leží mimo viewport (zobrazovací prostor), Qt s ním stále provádí nějaké výpočty, proto je vhodné takovéto trojúhelníky mazat, resp. nepředávat Qt k vykreslení.

## Řazení trojúhelníků

Při vytváření obecného renderovacího enginu je potřeba řešit pořadí vykreslování trojúhelníků, aby se nevykreslovaly vzdálenější objekty před bližšími objekty. V enginech se používá tzv. `z-buffer`, který ukládá vzdálenost každého pixelu od kamery pro správné vykreslování. V této hře ovšem toto řešení nepotřebuji, protože ve hře je pouze docela málo jednoduchých objektů a můžu správné pořadí kreslení určit už při programování. Nejdříve vykreslím arénu, potom jídlo a nakonec hada. Tím docílím, že had bude viditelný vždy, pokud bude kolidovat s jídlem, jídlo bude skryto "za hadem" a aréna bude skrytá za jakýmkoliv objektem v ní. Ušetřím tak výkon, protože nemusím nijak řadit trojúhelníky podle vzdálenosti.

# Herní cyklus

## Vlákna

Rozhodl jsem se pro celou aplikaci kvůli jednoduchosti využít dvě vlákna. Hlavní vlákno je využito jako renderovací vlákno. V něm probíhají veškeré transformace trojúhelníků a veškerá volání knihovny Qt. Druhé vlákno je herní vlákno, které drží stabilní tickrate a slouží k aktualizacím herního stavu. V tomto vlákně dochází k posunu hráče a k detekci kolizí jak s jídlem, tak s jinými objekty. Pokud had sežere jídlo, zvýší se jeho délka, rychlost a skóre. Pokud had narazí sám do sebe nebo do jiné překážky, hra skončí. Mezi vlákny je potřeba neustále přeposílat data, k tomu slouží metody chráněné `mutex`y.

## Vstupy

Had se ovládá za pomoci myši. Kam se hráč dívá, tím směrem se had pohybuje. Aby ovšem hráč nemohl manipulovat s hadem nereálně, má otáčení hada zpoždění za pohybem kamery. Maximální povolený rozdíl mezi směry za jeden tick je 10 bodů, což odpovídá přibližně 0,1 radiánu. Pokud je změna větší, ořízne se. Dále aktuální otočení hada se nastaví na 70 % staré hodnoty otočení + 30 % nové hodnoty otočení.

```c++
int toChange = newDir-oldDir;
if(toChange > 10) toChange = 10; //oriznuti
if(toChange < -10) toChange = -10; //oriznuti
newDir = oldDir + toChange;
this->moveDir = 0.7*oldDir + 0.3*newDir; //zpozdeni
```

Otáčení hada má tak jakýsi integrační charakter - je zpožděné za otočením kamery.

Dalším ovládacím prvkem je kolečko myši. Otáčením kolečka myši lze měnit výšku kamery. Samozřejmě pouze v nějakém rozmezí. Jiné ovládací prvky zatím ve hře nejsou.

## Modely

### Had

Model hada je procedurálně generovaný. Had by ideálně měl vypadat jako válec, na koncích užší než uprostřed (tedy spíše jakýsi kužel). Válec má ovšem nekonečno hran, což není vhodné pro renderování. Válec se tedy dá nahradit n-bokým hranolem (resp. jehlanem). Vyzkoušel jsem 9 boků a výsledek vypadá přijatelně. Samozřejmě čím více boků bude použito, tím lépe (tím kulatěji) bude tělo hada vypadat, ale tím více trojúhelníků bude generováno a tím vyšší výkon bude potřeba pro stejný framerate. Had je generován tak, že se skládá z několika článků. Každý článek okolo sebe vygeneruje n-úhelník s poloměrem podle toho, o který článek se zrovna jedná - uprostřed těla hada bude had "nejtlustší", směrem ke konci se bude zmenšovat. Pravidelný n-úhelník lze procedurálně vytvořit pomocí funkcí sinus a cosinus.
![Nákres generování devítiúhelníku](https://i.imgur.com/0Jofp8S.png)

### Jídlo

Ve hře jsou dva druhy jídla. Jablko a lektvar. Oba modely jsem vymodeloval se svými omezenými schopnostmi v programu Blender, vyexportoval je jako `.ply` soubor a pomocí vlastního PHP skriptu jsem je převedl na můj vlastní formát modelu. Modely jsou uloženy ve složce `models/` a načítány při spuštění hry třídou `ModelLoader`. Tato třída v sobě modely uchovává ve formě `vectorů` s trojúhelníky. 

