#include "ScreenWidget.h"
#include "DataStorage.h"

ScreenWidget::ScreenWidget(QWidget* parent) : QWidget(parent){
    this->layout = new QBoxLayout(QBoxLayout::Direction::TopToBottom);
    //this->menu = new MenuWidget();
    //this->layout->addWidget(this->menu);
    this->resize(1800, 800);
    //this->layout->setMargin(100);
    this->layout->setSpacing(0);
    //this->setWindowTitle("Snake - Main menu");
    this->setLayout(this->layout);
    this->displayMenu();
}

void ScreenWidget::deleteMenu(){
    std::cout << " this = " << this << std::endl;
    std::cout << " this->menu = " << this->menu << std::endl;
    std::cout << " count = " << this-> menu-> getLayout()-> count() << std::endl;
    //((QPushButton*)this->menu->getLayout()->itemAt(0))->setText(QString("hey"));
    //QTimer::singleShot(500, this, SLOT(removeFinal()));
    //this->layout->removeWidget(this->menu);
    std::cout << " 1 this->layout->count() = " << this->layout->count() << std::endl;
    this->menu->deleteLater();
    this->menu = nullptr;
    std::cout << " 2 this->layout->count() = " << this->layout->count() << std::endl;
    //this->layout->deleteLater();
    //this->layout = nullptr;
    //delete this->layout;
    //this->layout->removeWidget(this->layout->takeAt(0));
    //QLayoutItem *item = layout->itemAt(0);
    //qDeleteAll(this->layout->findChildren<QWidget*>("", Qt::FindDirectChildrenOnly));
    //layout->removeWidget(item->widget());
    /*if(this->menu != nullptr){
        delete this->menu;
        this->menu = nullptr;
    }*/
}

void ScreenWidget::displayGame(){
    DataStorage* ds = DataStorage::getInstance();
    ds->initSnakeData();
    CanvasWidget *canvas = new CanvasWidget();
    ds->setCanvasWidgetInstance(canvas);
    this->layout->setMargin(0);
    this->layout->addWidget(ds->getCanvasWidgetInstance());
    ds->getCanvasWidgetInstance()->startLoop();
    this->setWindowTitle("Snake - Hra");
}

void ScreenWidget::displayMenu(){
    this->menu = new MenuWidget();
    this->layout->addWidget(this->menu);
    this->layout->setMargin(100);
    this->setWindowTitle("Snake - Hlavní menu");
    std::cout << " ScreenWidget::displayMenu()" << std::endl;
    int score = DataStorage::getInstance()->getScore();
    if(score != 0){
        QLabel *label = new QLabel(QString("Konec hry, skóre: %1").arg(score), this->menu);
        this->menu->getLayout()->addWidget(label);
        this->menu->getLayout()->setStretch(2, 1);
    }
}

void ScreenWidget::deleteGame(){
    DataStorage* ds = DataStorage::getInstance();
    if(ds->getCanvasWidgetInstance() != nullptr){
        ds->getCanvasWidgetInstance()->deleteLater();
        ds->setCanvasWidgetInstance(nullptr);
    }
}

void ScreenWidget::resizeEvent(QResizeEvent *event){
    if(this->menu != nullptr){
        int x = (this->size().width()-this->menu->size().width()) / 2;
        int y = (this->size().height()-this->menu->size().height()) / 2;
        this->menu->move(x, y);
        QWidget::resizeEvent(event);
    }
}

void ScreenWidget::removeFinal(){
    this->layout->removeWidget(this->menu);
}
