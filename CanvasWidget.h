#ifndef CANVASWIDGET_H_
#define CANVASWIDGET_H_


#include <QWidget>
#include <QPainter>
#include <QPainterPath>
#include <QLinearGradient>
#include <QColor>
#include <QPen>
#include <QFont>
#include <QRectF>
#include <QTimer>
#include <QMouseEvent>
#include <QWheelEvent>
#include <QKeyEvent>
#include "Triangle.h"
#include "Triangle3D.h"
#include "GameThread.h"
#include "SmartMeasure.h"
#include <iostream>
#include <chrono>
#include <cmath>


class CanvasWidget : public QWidget {
    Q_OBJECT
    
    long tick;
    int fps;
    int fpsTarget;
    unsigned char debugType;
    std::chrono::milliseconds lastFpsShown;
    int fpsFinal;
    int tpsFinal;
    int ticksLast;
    std::vector<Triangle3D*> arenaTriangles;
    //int lastMouseX, lastMouseY;
    int cameraHeight;
    QTimer* timer;
    SmartMeasure* sm;
    std::vector<TimePoint*> pts;
    
    float e;
    bool tweak;
    bool doNotMoveMouse;
    int lastMouseX;
    int lastMouseY;
    
public:
    CanvasWidget(QWidget* parent = 0);
    void startLoop();
    ~CanvasWidget();
    
protected:
    void paintEvent(QPaintEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    void wheelEvent(QWheelEvent *event) override;
    void keyPressEvent(QKeyEvent *event) override;
    
    
};

#endif