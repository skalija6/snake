#include "MenuWidget.h"
#include "DataStorage.h"

MenuWidget::MenuWidget(QWidget* parent) : QWidget(parent){
    auto& self = *this;
    this->v = new QVBoxLayout();
    v->setMargin(0);
    v->setSpacing(100);
    
    //this->b = new QPushButton("Tlacitko");
    
    //QObject
    // propojeni signalu clicked emitovaneho objektem b se slotem
    //self.connect(this->b, SIGNAL(clicked()), this, SLOT(handleClick()));
    
    QPushButton* b = new QPushButton("Hrát");
    this->connect(b, SIGNAL(clicked()), this, SLOT(startGame()));
    v->addWidget(b);
    
    /*b = new QPushButton("Nastavení");
    this->connect(b, SIGNAL(clicked()), this, SLOT(handleClick()));
    v->addWidget(b);*/
    //this->h->addWidget(this->b);
    
    v->setStretch(0, 10);
    v->setStretch(1, 10);
    
    self.setLayout(v);
}

QVBoxLayout *MenuWidget::getLayout(){
    return this->v;
}

void MenuWidget::startGame(){
    DataStorage* ds = DataStorage::getInstance();
    ds->getScreenWidgetInstance()->deleteMenu();
    ds->getScreenWidgetInstance()->displayGame();
}

void MenuWidget::handleClick(){
    QPushButton* tmp = (QPushButton*)sender();
    //tmp->setStyleSheet("width: 50px; height: 50px; border: 0px; background: red");
    //tmp->updateState(0-tmp->getState());
  
    DataStorage* ds = DataStorage::getInstance();
    tmp->setText(QString(ds->getText().c_str()));
    
    tmp->setStyleSheet("background: green;");
}

void MenuWidget::handleResize(){
}

void MenuWidget::resizeEvent(QResizeEvent *event){
    if(this->parentWidget() != 0){
        int x = (this->parentWidget()->size().width()-this->size().width()) / 2;
        int y = (this->parentWidget()->size().height()-this->size().height()) / 2;
        this->move(x, y);
    }
    QWidget::resizeEvent(event);
}