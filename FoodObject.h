#ifndef FOODOBJECT_H_
#define FOODOBJECT_H_


#include "DataStorage.h"
#include "Triangle3D.h"
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <time.h>
#include <string>

class FoodObject {
public:
    int x;
    int y;
    std::string modelName;
    FoodObject(int x, int y, std::string m = "apple");
    std::vector<Triangle3D*> getModel();
    
};

#endif