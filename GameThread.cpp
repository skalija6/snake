#include "GameThread.h"

void GameThread::run(){
    const int tickrate = 50;
    DataStorage *ds = DataStorage::getInstance();
    ds->clearTick();
    while(true){
        if(ds->getGameState() == 2){
            //Game over
            break;
        }

        //Increment tick
        int currentTick = ds->increment();
        
        //Update player facing (delayed after mouse input)
        ds->updateFacing();
        
        //Update player position
        std::pair<int,int> viewXY = ds->getFacing();
        int speed = ds->getPlayerSpeed();
        float alpha = viewXY.first/100.0;
        int dx = speed*cos(alpha);
        int dy = -speed*sin(alpha);
        ds->updatePlayerPos(dx, dy);
        ds->updateSnakeData(viewXY.first, speed);
        
        //Check for collisions
        ds->checkCollisions();
        
        //Spawn food
        if(currentTick % (tickrate*10) == 0){ //Every 10 seconds
            std::cout << currentTick << " % " << tickrate*10 << " == 0" << std::endl;
            ds->addFoodObject();
        }
    
        //Tick limiting
        QThread::msleep(1000/tickrate);
    }
}

GameThread::GameThread(QWidget* parent) : QThread(parent){
    std::cout << " Created GameThread(" << parent << ") : " << this << std::endl;
}

GameThread::~GameThread(){
    DataStorage *ds = DataStorage::getInstance();
    ds->clearFoodObjects();
    std::cout << " Killed ~GameThread() : " << this << std::endl;
}
