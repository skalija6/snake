#include <QApplication>
#include <QLabel>
#include <QPushButton>
#include <QHBoxLayout>
#include <time.h>
#include <chrono>
#include "ScreenWidget.h"
#include "DataStorage.h"

int main(int argc, char* argv[]){
  QApplication app(argc, argv);
  
  srand(time(NULL));
  
  /*QWidget w; //container
  w.resize(600, 400);
  w.setWindowTitle("Snake - Main menu");*/
  
  ScreenWidget *screen = new ScreenWidget();
  std::cout << " main::screen = " << screen << std::endl;
  DataStorage* ds = DataStorage::getInstance();
  std::cout << " main::ds = " << ds << std::endl;
  ds->setScreenWidgetInstance(screen);
  
  /*QPushButton b("Hrát", &w);
  b.setStyleSheet(QString("background-color: #55ff66; padding: 5px;"));*/
  
  /*int x = (w.size().width()-menu->size().width()) / 2;
  int y = (w.size().height()-menu->size().height()) / 2;
  menu->move(x, y);*/
  
  screen->show();
  //w.show(); //show the container
  
  return app.exec();
}