#include "SnakeData.h"

struct Point3D{
    int x, y, z;
    Point3D(int x, int y, int z) : x(x), y(y), z(z){}
};

SnakeData::SnakeData(int headX, int headY, int direction, int length){
    this->partLen = 150;
    this->partWidthRatio = 2;
    float alpha = direction / 100.0;
    //Create snake's body parts
    for(int i=0; i<length; i++){
        int partX = headX - i * this->partLen*sin(alpha);
        int partY = headY - i * this->partLen*cos(alpha);
        this->snakeParts.push_back(std::pair<int,int>(partX, partY));
    }
    this->facingDirection = 628-direction;
}

void SnakeData::moveSnake(int direction, int distance, bool createNewPart){
    //std::cout << "moveSnake: createNewPart ? " << (createNewPart ? "TRUE" : "FALSE") << std::endl;
    float alpha = direction / 100.0;
    this->facingDirection = direction;
    int dx = distance*cos(alpha);
    int dy = -distance*sin(alpha);
    int lastX = 0, lastY = 0;
    for(unsigned int i = 0; i < this->snakeParts.size(); i++){
        if(i == 0){
            //first part -> move in the direction
            lastX = snakeParts[0].first;
            lastY = snakeParts[0].second;
            this->snakeParts[0].first += dx;
            this->snakeParts[0].second += dy;
        }else if(i+1 < this->snakeParts.size()){
            //not the first nor the last part -> move in direction of previous part
            int deltaX = lastX-this->snakeParts[i].first;
            int deltaY = lastY-this->snakeParts[i].second;
            lastX = snakeParts[i].first;
            lastY = snakeParts[i].second;
            int dstSq = deltaX*deltaX + deltaY*deltaY; //distance from previous body part
            if(dstSq < this->partLen*this->partLen){
                //move only smaller amount
                distance *= ((float)dstSq) / (this->partLen*this->partLen);
            }
            //if(dstSq >= this->partLen*this->partLen){
                std::pair<int,int> dXY = getDirectionIncrease(distance, deltaX, deltaY);
                this->snakeParts[i].first += dXY.first;
                this->snakeParts[i].second += dXY.second;
            //}
        }else{
            //this is the last part
            int deltaX = lastX-this->snakeParts[i].first;
            int deltaY = lastY-this->snakeParts[i].second;
            int dstSq = deltaX*deltaX + deltaY*deltaY; //distance from previous body part
            if(dstSq >= this->partLen*this->partLen){
                //move this part
                std::pair<int,int> dXY = getDirectionIncrease(distance, deltaX, deltaY);
                lastX = snakeParts[i].first;
                lastY = snakeParts[i].second;
                this->snakeParts[i].first += dXY.first;
                this->snakeParts[i].second += dXY.second;
            }/*else if(createNewPart){
                //cannot create new snake part if the last one is still not distant enough from the rest of snake
                createNewPart = false;
            }*/
            
        }
    }//foreach snake part
    if(createNewPart){
        std::cout << "Adding new snake part!" << std::endl;
        this->snakeParts.push_back(std::pair<int,int>(lastX, lastY));
    }
}

std::pair<int,int> SnakeData::getDirectionIncrease(int dist, int dx, int dy){
    if(dx == 0){
        return std::pair<int,int>(0, dist);
    }
    if(dy == 0){
        return std::pair<int,int>(dist, 0);
    }
    float tan = ((float)dy)/dx;
    float newdx = ((float)dist) / sqrt(1+tan*tan);
    if(dx < 0) newdx *= -1;
    float newdy = tan*newdx;
    return std::pair<int,int>((int)newdx, (int)newdy);

}

std::vector<Triangle3D*> SnakeData::getModel(){
    const int ngon = 9;
    const int snakeHeightCenter = this->partLen * this->partWidthRatio;
    const int snakeHeightOffset = 1990-snakeHeightCenter;
    float sincos[ngon*2];
    for(int i=0; i<ngon; i++){
        float angle = (i*6.28319)/(float)ngon;
        sincos[2*i] = snakeHeightCenter*sin(angle);
        sincos[2*i+1] = snakeHeightCenter*cos(angle);
    }
    std::vector<Triangle3D*> model;
    std::vector<Point3D*> modelPoints;
    float lastAngleAverage = 0;
    static float maxDeltaAA = 0;
    for(unsigned int i = 0; i < this->snakeParts.size(); i++){
        std::pair<int,int> partXY = snakeParts[i];
        /*model.push_back(new Triangle3D(partXY.first, partXY.second, -1800, partXY.first-100, partXY.second-50, -1800,
          partXY.first-100, partXY.second+50, -1800, 100, 120, 255, 200));*/ //only for testing purpose
        float angleAverage = 0;
        if(i+1 < this->snakeParts.size()){
            //not last
            std::pair<int,int> nextPartXY = snakeParts[i+1];
            float dx = + nextPartXY.first - partXY.first;
            float dy = nextPartXY.second - partXY.second;
            angleAverage = atan2(dy, dx);
            //testing if(i!=0) angleAverage /= 2;
        }else{
            //last -> only testing
            std::pair<int,int> prevPartXY = snakeParts[i-1];
            float dx = + partXY.first - prevPartXY.first;
            float dy = partXY.second - prevPartXY.second;
            angleAverage = atan2(dy, dx);
        }
        /*if(i != 0){
            //not first
            std::pair<int,int> prevPartXY = snakeParts[i-1];
            float dx = + partXY.first - prevPartXY.first;
            float dy = partXY.second - prevPartXY.second;
            if(angleAverage != 0) angleAverage += atan2(dy, dx) / 2;
            else angleAverage += atan2(dy, dx);
        }*/
        //angleAverage = 6.28 - angleAverage;
        //std::cout << "angleAverage[" << i << "]=" << angleAverage << "    delta=" << angleAverage-lastAngleAverage << "    max=" << maxDeltaAA << std::endl;
        if(std::abs(angleAverage-lastAngleAverage) > maxDeltaAA) maxDeltaAA = std::abs(angleAverage-lastAngleAverage);
        if(i==0){
            //Snake head
            float alpha = (this->facingDirection+157) / 100.0;
            //std::cout << "  facing="<<alpha<<" rad angleAverage-facing="<<angleAverage-alpha<<std::endl;
            int headX = 100*sin(alpha) + partXY.first;
            int headY = 100*cos(alpha) + partXY.second;
            modelPoints.push_back(new Point3D(headX, headY, snakeHeightCenter-snakeHeightOffset)); //Face tip point
            /*model.push_back(new Triangle3D(headX, headY, snakeHeightCenter-snakeHeightOffset, headX-100, headY-50, snakeHeightCenter-snakeHeightOffset,
              headX-100, headY+50, snakeHeightCenter-snakeHeightOffset, 50, 30, 200, 200));*/ //only for testing purpose
        }else{
            /*if(angleAverage-lastAngleAverage > 2.5){
                angleAverage *= -1;
            }else if(lastAngleAverage-angleAverage > 2.5){
                angleAverage *= -1;
            }*/
        }
        //N-gon
        float sinA = sin(angleAverage);
        float cosA = cos(angleAverage);
        for(int j=0; j<ngon; j++){
            float pX = 0;
            float pY = 0;
            float pZ = snakeHeightCenter-snakeHeightOffset;
            //if(i == 0) std::cout  << "bef [" << j << "] -> " << pX << ", " << pY << ", " << pZ << std::endl;
            //in YZ plane do n-gon
            float r = 4*((float)this->snakeParts.size()-i) / (3*this->snakeParts.size());
            if(r > 1.0) r = 1.0;
            if(i==0) r = 0.7;
            if(i==1) r = 0.95;
            pY += r*sincos[2*j];
            pZ += r*sincos[2*j+1];
            //if(i == 0) std::cout  << "bef2[" << j << "] -> " << pX << ", " << pY << ", " << pZ << std::endl;
            //rotate in XY plane (do not touch Z)
            float newX = pX*cosA - pY*sinA;
            float newY = pX*sinA + pY*cosA;
            newX += partXY.first;
            newY += partXY.second;
            modelPoints.push_back(new Point3D(newX, newY, pZ));
            //if(i == 0) std::cout  << "push[" << j << "] -> " << newX << ", " << newY << ", " << pZ << std::endl;
        }
        lastAngleAverage = angleAverage;
    }
    //Snake head
    for(int j=0; j<ngon; j++){
        int f = j+1;
        int s = (j+1)%ngon + 1;
        ColorRGBA c = this->getTriangleColor(modelPoints[f]->x, modelPoints[f]->y, modelPoints[f]->z, modelPoints[s]->x, modelPoints[s]->y, modelPoints[s]->z);
        model.push_back(new Triangle3D(modelPoints[f]->x, modelPoints[f]->y, modelPoints[f]->z, modelPoints[s]->x, modelPoints[s]->y,
          modelPoints[s]->z, modelPoints[0]->x, modelPoints[0]->y, modelPoints[0]->z, c.r, c.g, c.b, c.a));
    }
    //Snake body
    for(unsigned int i=1+ngon; i<modelPoints.size(); i+=ngon){
        for(int j=0; j<ngon; j++){
            int f = i + j;
            int s = i + (j+1)%ngon;
            int f2 = f-ngon;
            int s2 = s-ngon;
            ColorRGBA c = this->getTriangleColor(modelPoints[f]->x, modelPoints[f]->y, modelPoints[f]->z, modelPoints[s]->x, modelPoints[s]->y, modelPoints[s]->z, i+j);
            model.push_back(new Triangle3D(modelPoints[f]->x, modelPoints[f]->y, modelPoints[f]->z, modelPoints[s]->x, modelPoints[s]->y,
              modelPoints[s]->z, modelPoints[f2]->x, modelPoints[f2]->y, modelPoints[f2]->z, c.r, c.g, c.b, c.a));
            ColorRGBA c2 = this->getTriangleColor(modelPoints[f2]->x, modelPoints[f2]->y, modelPoints[f2]->z, modelPoints[s]->x, modelPoints[s]->y, modelPoints[s]->z, i+j);
            model.push_back(new Triangle3D(modelPoints[f2]->x, modelPoints[f2]->y, modelPoints[f2]->z, modelPoints[s]->x, modelPoints[s]->y,
              modelPoints[s]->z, modelPoints[s2]->x, modelPoints[s2]->y, modelPoints[s2]->z, c2.r, c2.g, c2.b, c2.a));
        }
    }
    //int ii=0;
    for(Point3D* p : modelPoints){
        /*std::cout << ii++ << " -> " << p->x << ", " << p->y << ", " << p->z;
        if(ii%4 != 3) std::cout << "            ";
        else std::cout << std::endl;*/
        delete p;
    }
    return model;
}

std::vector< std::pair<int,int> > SnakeData::getSnakeParts(){
    return this->snakeParts;
}

ColorRGBA SnakeData::getTriangleColor(int x1, int y1, int z1, int x2, int y2, int z2, int k){
    float nx = y1*z2 - y2*z1;
    float ny = z1*x2 - z2*x1;
    float nz = x1*y2 - x2*y1;
    float ux = 2;
    float uy = 1;
    float uz = 8;
    float dotProduct = nx*ux + ny*uy + nz*uz; // range -1 .. 1
    dotProduct = dotProduct / (std::sqrt(nx*nx + ny*ny + nz*nz) * std::sqrt(ux*ux + uy*uy + uz*uz));
    //float d = dotProduct;
    //if(d<0) d=0; 
    float d = (dotProduct + 1.0)/4.0 + 0.5;
    d = (z1+z2) / 4000.0 + 1.0;
    d *= 2.5;
    if(d>1) d = 1;
    //std::cout << d << std::endl;
    //if((k/ngon)%2) d = 0.7;
    //if(k%2) d = 0.4;
    return ColorRGBA(d*200, d*30, d*0, 255);
}

