#include "DataStorage.h"
#include <stdexcept>

DataStorage* DataStorage::instance = nullptr;

DataStorage::DataStorage(){
    if(instance != nullptr){
        throw std::invalid_argument("Cannot create multiple instances of DataStorage!");
    }
    instance = this;
    this->snakeData = nullptr;
    this->initSnakeData();
    this->modelLoader = new ModelLoader();
    
    unsigned long micros = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
    this->randomSeed = ((micros+5000)/2000)%100;
    if(this->randomSeed == 0) this->randomSeed = 2;
    if(this->randomSeed < 0) this->randomSeed *= -1;
    
    this->menuState = 0;
}

DataStorage* DataStorage::getInstance(){
    if(instance == nullptr){
        new DataStorage();
    }
    return instance;
}

void DataStorage::setScreenWidgetInstance(ScreenWidget* sw){
    this->screen = sw;
    std::cout << " DataStorage::setS(" << sw << ")" << std::endl;
}

ScreenWidget* DataStorage::getScreenWidgetInstance(){
    std::cout << " DataStorage::getS() = " << this->screen << std::endl;
    return this->screen;
}

void DataStorage::setCanvasWidgetInstance(CanvasWidget* cw){
    this->canvas = cw;
    std::cout << " DataStorage::setC(" << cw << ")" << std::endl;
}

CanvasWidget* DataStorage::getCanvasWidgetInstance(){
    std::cout << " DataStorage::getC() = " << this->canvas << std::endl;
    return this->canvas;
}

void DataStorage::setGameThreadInstance(GameThread* gt){
    this->gameThread = gt;
    std::cout << " DataStorage::setGT(" << gt << ")" << std::endl;
}

GameThread* DataStorage::getGameThreadInstance(){
    std::cout << " DataStorage::getGT() = " << this->gameThread << std::endl;
    return this->gameThread;
}

ModelLoader* DataStorage::getModelLoader(){
    return this->modelLoader;
}

std::string DataStorage::getText(){
    return std::string("Jop, kliknuto.");
}

int DataStorage::increment(){
    this->mutex.lock();
    int ret = this->sync++;
    this->mutex.unlock();
    return ret;
}

int DataStorage::get(){
    this->mutex.lock();
    int s = this->sync;
    this->mutex.unlock();
    return s;
}

void DataStorage::clearTick(){
    this->mutex.lock();
    this->sync = 0;
    this->mutex.unlock();
}

void DataStorage::initSnakeData(){
    this->mutex.lock();
    this->sync = 0;
    this->viewX = 0;
    this->viewY = 0;
    this->moveDir = 0;
    this->playerScore = 0;
    this->playerSpeed = 30;
    this->playerPosX = 5000;
    this->playerPosY = 2000;
    this->playerLength = 48;
    this->addParts = 0;
    if(this->snakeData != nullptr){
        delete this->snakeData;
    }
    this->snakeData = new SnakeData(this->playerPosX, this->playerPosY, 157, this->playerLength);
    this->gameState = 0;
    this->playerHasSlownessPowerup = false;
    this->slownessPowerupActiveTicksLeft = 0;
    this->mutex.unlock();
}

void DataStorage::updateView(int dx, int dy){
    this->mutex.lock();
    this->viewX += dx;
    if(this->viewX >= 628) this->viewX -= 628;
    if(this->viewX < 0) this->viewX += 628;
    this->viewY += dy;
    if(this->viewY > 157) this->viewY = 157;
    if(dy > -15 && this->viewY > -170) if(this->viewY < -157) this->viewY = -157;
    this->mutex.unlock();
}

std::pair<int,int> DataStorage::getView(){
    this->mutex.lock();
    std::pair<int,int> p(this->viewX, this->viewY);
    this->mutex.unlock();
    return p;
}

std::pair<int,int> DataStorage::getFacing(){
    this->mutex.lock();
    std::pair<int,int> p(this->moveDir, this->viewY);
    this->mutex.unlock();
    return p;
}

void DataStorage::updateFacing(){
    this->mutex.lock();
    int oldDir = this->moveDir;
    int newDir = this->viewX;
    if(oldDir-newDir > 314 && oldDir-628-newDir < 314){
        oldDir -= 628;
    }else if(newDir-oldDir > 314 && newDir-628-oldDir < 314){
        oldDir += 628;
    }/*else if(this->moveDir > 628){
        this->moveDir -= 628;
    }else if(this->moveDir < 0){
        this->moveDir += 628;
    }*/
    
    int toChange = newDir-oldDir;
    if(toChange > 10) toChange = 10;
    if(toChange < -10) toChange = -10;
    
    newDir = oldDir + toChange;
    this->moveDir = 0.7*oldDir + 0.3*newDir;
    this->mutex.unlock();
}

std::pair<int,int> DataStorage::getPlayerPos(){
    this->mutex.lock();
    std::pair<int,int> p(this->playerPosX, this->playerPosY);
    this->mutex.unlock();
    return p;
}

void DataStorage::setPlayerPos(int x, int y){
    this->mutex.lock();
    this->playerPosX = x;
    this->playerPosY = y;
    this->mutex.unlock();
}

void DataStorage::updatePlayerPos(int dx, int dy){
    this->mutex.lock();
    this->playerPosX += dx;
    this->playerPosY += dy;
    this->mutex.unlock();
}

void DataStorage::updateSnakeData(int direction, int speed){
    this->mutex.lock();
    bool addPart = false;
    //std::cout << "updateSnakeData: addParts: " << this->addParts << std::endl;
    if(this->addParts > 0){
        addPart = true;
        this->addParts--;
    }
    this->snakeData->moveSnake(direction, speed, addPart);
    this->mutex.unlock();
}

int DataStorage::getPlayerSpeed(bool gameTick){
    this->mutex.lock();
    int p = this->playerSpeed;
    //as this is called once every tick:
    if(this->slownessPowerupActiveTicksLeft > 0){
      p -= 50;
      if(p < 10) p = 10;
      if(gameTick) this->slownessPowerupActiveTicksLeft--;
    }
    this->mutex.unlock();
    return p;
}

void DataStorage::debugResetPlayerSpeed(){
    this->mutex.lock();
    this->playerSpeed = 0;
    this->mutex.unlock();
}

std::vector<Triangle3D*> DataStorage::getSnakeModel(){
    this->mutex.lock();
    std::vector<Triangle3D*> model = this->snakeData->getModel();
    this->mutex.unlock();
    return model;
}

void DataStorage::setGameState(int s){
    this->mutex.lock();
    this->gameState = s;
    this->mutex.unlock();
}

int DataStorage::getGameState(){
    this->mutex.lock();
    int s = this->gameState;
    this->mutex.unlock();
    return s;
}

void DataStorage::checkCollisions(){
    const int FOOD_ADD_LENGTH = 4;
    this->mutex.lock();
    const int bound = 800*30;
    const int snakeThickness = 270;
    unsigned char collision = 0;
    if(this->playerPosX-snakeThickness < 0 || this->playerPosX+snakeThickness > bound) collision = 1;
    else if((this->playerPosY-snakeThickness < 0 || this->playerPosY+snakeThickness > bound)) collision = 2;
    
    //if colided, end game
    if(collision != 0){
        this->gameState = 2;
    }else{
        //check other collisions
        std::vector< std::pair<int,int> > snakeParts = this->snakeData->getSnakeParts();
        for(unsigned int i=2; i<snakeParts.size(); i++){
            float r = 4*((float)snakeParts.size()-i) / (3*snakeParts.size());
            if(r > 1.0) r = 1.0;
            int dx = this->playerPosX - snakeParts.at(i).first;
            int dy = this->playerPosY - snakeParts.at(i).second;
            int distSq = dx*dx + dy*dy;
            if(distSq < (int)(r*snakeThickness*snakeThickness)){
                collision = 3;
                break;
            }
        }
        if(collision != 0){
            this->gameState = 2;
        }
    }
    //check food collisions
    std::vector<unsigned int> foodToDelete;
    bool addAnotherFood = false;
    for(unsigned int i=0; i < this->foodObjects.size(); i++){
        //foreach food
        int dx = this->playerPosX - foodObjects.at(i)->x;
        int dy = this->playerPosY - foodObjects.at(i)->y;
        int distSq = dx*dx + dy*dy;
        if(distSq < (int)(5*snakeThickness*snakeThickness)){ //colided
            //eat food
            std::string foodType = foodObjects.at(i)->modelName;
            foodToDelete.push_back(i);
            std::cout << "Food '" << foodType << "' eaten!" << std::endl;
            this->playerScore += 10;
            if(foodType == "apple"){
                this->playerSpeed += 80.0/(this->playerSpeed-3);
                this->playerLength += FOOD_ADD_LENGTH;
                this->addParts += FOOD_ADD_LENGTH;
                if(this->foodObjects.size() < 3) addAnotherFood = true;
            }else if(foodType == "potion"){
                this->playerHasSlownessPowerup = true;
            }
            break; //only eat one food item at once
        }
    }
    int deleted = 0;
    for(unsigned int i : foodToDelete){
        try{
            delete foodObjects.at(i); //makes the program crash -> Thread 21 received signal SIGTRAP, Trace/breakpoint trap.
            /**
             * [Switching to Thread 20196.0x289c]
0x00007ff898a791b3 in ntdll!RtlIsNonEmptyDirectoryReparsePointAllowed ()
   from C:\Windows\SYSTEM32\ntdll.dll
(gdb) info stack
#0  0x00007ff898a791b3 in ntdll!RtlIsNonEmptyDirectoryReparsePointAllowed ()
   from C:\Windows\SYSTEM32\ntdll.dll
#1  0x00007ff898a815e2 in ntdll!RtlpNtMakeTemporaryKey ()
   from C:\Windows\SYSTEM32\ntdll.dll
#2  0x00007ff898a818ea in ntdll!RtlpNtMakeTemporaryKey ()
   from C:\Windows\SYSTEM32\ntdll.dll
#3  0x00007ff898a8a8a9 in ntdll!RtlpNtMakeTemporaryKey ()
   from C:\Windows\SYSTEM32\ntdll.dll
#4  0x00007ff898a2642d in ntdll!memset () from C:\Windows\SYSTEM32\ntdll.dll
#5  0x00007ff8989bfb91 in ntdll!RtlFreeHeap ()
   from C:\Windows\SYSTEM32\ntdll.dll
#6  0x00007ff897159cfc in msvcrt!free () from C:\Windows\System32\msvcrt.dll
#7  0x00000000004035ca in DataStorage::checkCollisions (this=0xd115c0)
    at DataStorage.cpp:252
#8  0x0000000000406a92 in GameThread::run (this=0x733e2f0)
    at GameThread.cpp:30
#9  0x00000000688a2c0c in QThreadPrivate::start(void*) ()
   from C:\Users\admin\Documents\Qt\PPC_had\snake\debug\Qt5Core.dll
#10 0x00007ff897037bd4 in KERNEL32!BaseThreadInitThunk ()
   from C:\Windows\System32\kernel32.dll
#11 0x00007ff8989ece51 in ntdll!RtlUserThreadStart ()
   from C:\Windows\SYSTEM32\ntdll.dll
#12 0x0000000000000000 in ?? ()
Backtrace stopped: previous frame inner to this frame (corrupt stack?)
             * 
             */
            foodObjects.erase(foodObjects.begin()+i-deleted);
            deleted++;
        }catch(std::out_of_range e){
            std::cout << e.what() << std::endl;
        }
    }
    if(addAnotherFood) this->addLocalFoodObject();
    this->mutex.unlock();
}

void DataStorage::addFoodObject(int x, int y){
    if(this->foodObjects.size() >= 3) return;
    if(x == -99999 && y == -99999){
        x = ((this->nextRandomInt() % 28)+1) * 800;
        y = ((this->nextRandomInt() % 28)+1) * 800;
    }
    this->mutex.lock();
    this->foodObjects.push_back(new FoodObject(x, y, "apple"));
    this->mutex.unlock();
    std::cout << "Spawning new food at " << x << ", " << y << std::endl;
}

void DataStorage::addLocalFoodObject(int x, int y){ //private function
    bool spawnPotionInstead = false;
    int r = this->nextRandomInt();
    std::cout << "rand: " << r << std::endl;
    if(r % 100 < 30) spawnPotionInstead = true;
    if(x == -99999 && y == -99999){
        x = ((this->nextRandomInt() % 28)+1) * 800;
        y = ((this->nextRandomInt() % 28)+1) * 800;
    }
    this->foodObjects.push_back(new FoodObject(x, y, (spawnPotionInstead ? "potion" : "apple")));
    std::cout << "Spawning new food at " << x << ", " << y << std::endl;
}

void DataStorage::clearFoodObjects(){
    this->mutex.lock();
    for(FoodObject* fo : this->foodObjects){
        delete fo;
    }
    this->foodObjects.clear();
    this->mutex.unlock();
}

std::vector<Triangle3D*> DataStorage::getFoodModel(){
    this->mutex.lock();
    std::vector<Triangle3D*> model;
    for(unsigned int i=0; i < this->foodObjects.size(); i++){
        for(Triangle3D* t : this->foodObjects.at(i)->getModel()){
            model.push_back(t);
        }
    }
    this->mutex.unlock();
    return model;
}

int DataStorage::getFoodObjectCount(){
    this->mutex.lock();
    int s = this->foodObjects.size();
    this->mutex.unlock();
    return s;
}

int DataStorage::getScore(){
    this->mutex.lock();
    int s = this->playerScore;
    this->mutex.unlock();
    return s;
}

bool DataStorage::hasSlownessPowerup(){
    this->mutex.lock();
    bool s = this->playerHasSlownessPowerup;
    this->mutex.unlock();
    return s;
}

void DataStorage::claimSlownessPowerup(){
    this->mutex.lock();
    this->playerHasSlownessPowerup = false;
    this->slownessPowerupActiveTicksLeft = SLOWNESS_POWERUP_TICKS;
    this->mutex.unlock();
}

double DataStorage::getSlownessPowerupDuration(){
    this->mutex.lock();
    double ret = 1.0;
    if(!this->playerHasSlownessPowerup){
        ret = (double)(this->slownessPowerupActiveTicksLeft) / (double)SLOWNESS_POWERUP_TICKS;
    }
    this->mutex.unlock();
    return ret;
}

int DataStorage::nextRandomInt(){ //private function
    int r = rand();
    std::cout << "RAND=" << r << ", SEED=" << this->randomSeed << ", mult=" << (int)(r * this->randomSeed) << std::endl;
    long better = r;
    better *= this->randomSeed;
    better = better % 2000000000;
    return better;
}

unsigned short& DataStorage::getMenuState(){
    return this->menuState;
}
