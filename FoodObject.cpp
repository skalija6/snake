#include "FoodObject.h"

FoodObject::FoodObject(int x, int y, std::string m) : x(x), y(y), modelName(m) {}

std::vector<Triangle3D*> FoodObject::getModel(){
    std::vector<Triangle3D*> model;
    //model.push_back(new Triangle3D(x, y, -1800, x+1000, y, -1800, x, y+1000, -1800, 200, 230, 100));
    //model.push_back(new Triangle3D(x, y, -1800, x-1000, y, -1800, x, y-1000, -1800, 200, 230, 100));
    int z = -1800;
    
    DataStorage* ds = DataStorage::getInstance();
    
    auto modelTemplate = ds->getModelLoader()->getModel(this->modelName);
    
    for(Triangle3D* &t : modelTemplate){
        Triangle3D* newT = new Triangle3D(t->x1 + x, t->y1 + y, t->z1 + z, t->x2 + x, t->y2 + y, t->z2 + z, t->x3 + x, t->y3 + y, t->z3 + z,
            t->color[0], t->color[1], t->color[2], t->color[3]);
        model.push_back(newT);
    }
    
    return model;
}