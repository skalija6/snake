#ifndef SCREENWIDGET_H_
#define SCREENWIDGET_H_


#include <QWidget>
#include <QLayout>
#include <QBoxLayout>
#include <QTimer>
#include <QMessageBox>
#include <QLabel>
#include "MenuWidget.h"
#include "CanvasWidget.h"
#include <iostream>


class ScreenWidget : public QWidget {
    Q_OBJECT
    
    QBoxLayout *layout;
    MenuWidget *menu;
    
public:
    ScreenWidget(QWidget* parent = 0);
    ScreenWidget(const ScreenWidget& c) = delete;
    void deleteMenu();
    void displayGame();
    void displayMenu();
    void deleteGame();
    
protected:
    void resizeEvent(QResizeEvent *event);

public slots:
    void removeFinal();
    
};

#endif