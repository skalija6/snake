#ifndef SNAKEDATA_H_
#define SNAKEDATA_H_

#include "Triangle3D.h"
#include <vector>
#include <cmath>
#include <iostream>
#include <QColor>

struct ColorRGBA {
    unsigned char r, g, b, a;
    ColorRGBA(unsigned char r, unsigned char g, unsigned char b, unsigned char a) 
      : r(r), g(g), b(b), a(a) {}
};

class SnakeData{
    std::vector< std::pair<int,int> > snakeParts; // idx 0 => snake head
    int partLen;
    float partWidthRatio;
    int facingDirection;
    unsigned char color[4];
    ColorRGBA getTriangleColor(int x1, int y1, int z1, int x2, int y2, int z2, int k = -1);
    
public:
    SnakeData(int headX, int headY, int direction, int length=6);
    void moveSnake(int direction, int distance, bool createNewPart = false);
    std::pair<int,int> getDirectionIncrease(int dist, int dx, int dy);
    std::vector<Triangle3D*> getModel();
    std::vector< std::pair<int,int> > getSnakeParts();
};





#endif